/*
This is the first attempt at a System of Equations program that takes in inputs from user and solves for the provided variables.
This version is very heavy on user interaction and asks a lot of question to create the formula which will be solved. Next step to making this
simpler would be using strings and solving the equation based on that.
*/

#include <iostream>
#include "CoVar.h"

using namespace std;

char XYZ[3] = {'X','Y','Z'};

/*
This is the function that the majority of the program is ran through it's separate function so that the arrays used to build equations don't remain in use after they've been solved
Function Input: This function does not take in any inputs.
Function Output: The input is void however inside the function the user will interact with the program
*/
void run();
/*
This is the function where the user will inputs the values for each function
Function Input: The inputs for this function are vars, how many variables there are, and *temp which is a pointer to a temporary array used to transfer values to the
coeffectuan and solution arrays.
Function Output: The function doesn't return anything to the user but it does provide necessary information to the constant and solution matrixes.
*/
void equation(int vars, double *temp);
/*
This function is here to display the visual representation of the system of equation that the user has created
Function Input: The inputs to this function are a pointer to the first value in the 2D array representing the constant Matrix, a pointer to the first value in solution Matrix and the dimensions of the matrixes
Function Output: No proper output, however it will print out a visual representation of the System of Equation in Matrix form.
*/
void printSystemOfEquations(double *constantPointer, double *solutionMatrix,int dimension);
/*
This function takes the given system of equations and then solves it for each of the individual unknown variables.
Function Input: The inputs to this function are a pointer to the first value in the 2D array representing the constant Matrix, a pointer to the first value in solution Matrix and the dimensions of the matrixes
Function Output: Gives back the value of the provided constant matrix determinant.
*/
double calculateDet(double *constantPointer, double *solutionMatrix,int dimension);

int main()
{
    char input;
    cout << "Welcome to the System of Equations Program, here you will enter one to three equations and we'll take care of the \nannoying algebra by solving for the variables." << endl;
mainJump:
    run();
    cout << "Would you like to do another problem? (Y/N)" << endl;
mainJump1:
    cin >> input;
    if(input == 'Y')
    {
        goto mainJump;
    }
    else if (input == 'N'){}
    else
    {
        cout << "Sorry, I did not understand please enter Y for Yes or N for No." << endl;
        goto mainJump1;
    }

    return 0;
}

void run()
{
    char inputChar = 'C';
    int Varibles = 0;
    double temp [4] = {0,0,0,0};
    double tempArray[4] = {0,0,0,0};
    double *pointer = &temp[0];
    double *pointerC;
    double *pointerS;
    cout << "How many Variables do you need to solve for?(1-3)" << endl;
JumpRun:
    cin >> Varibles;
    if(Varibles < 1 || Varibles > 3)
    {
        cout << "This program can only work with 1,2 or 3 variables at a time only enter, 1 2 or 3 to continue" << endl;
        goto JumpRun;
    }
    double constantMatrix [Varibles][Varibles];
    double solutionMatrix [Varibles];

    for(int i = 0; i < Varibles; i++)
    {
        cout << "Equation #" << i+1 << ":" << endl;
        equation(Varibles, pointer);
        solutionMatrix[i] = temp[0];
        constantMatrix[i][0] = temp[1];
        constantMatrix[i][1] = temp[2];
        constantMatrix[i][2] = temp[3];
        temp [0] = 0;
        temp [1] = 0;
        temp [2] = 0;
        temp [3] = 0;
    }

    pointerC = &constantMatrix[0][0];
    pointerS = &solutionMatrix[0];

    printSystemOfEquations(pointerC,pointerS,Varibles);

    temp[0] = calculateDet(pointerC, pointerS, Varibles);
    if(temp[0] == 0 && Varibles > 1)
    {
        for(int i = 1; i < Varibles+1; i++)
        {
            cout << XYZ[i] << " = Does Not Exist" << endl;
        }
    }
    else if(Varibles == 1){}
    else
    {
        for(int i = 0; i < Varibles; i++)
        {
            for(int j = 0; j < Varibles; j++)
                {
                    tempArray[j] = constantMatrix[j][i];
                    constantMatrix[j][i] = solutionMatrix[j];
                }
            temp[i+1] = calculateDet(pointerC, pointerS, Varibles);

            for(int k = 0; k < Varibles; k++)
            {
                constantMatrix[k][i] =  tempArray[k];
            }
        }

        for(int i = 1; i < Varibles+1; i++)
        {
            cout << XYZ[i-1] <<  " = " << temp[i]/temp[0] << endl;
        }
    }
}

void equation(int vars, double *temp)
{
    int inputInt = 0;
    char inputChar = 'C';
    int nVars = vars;
    int LHSSize = 0;
    int RHSSize = 0;
    cout << "How many inputs will you have on the Left Hand Side?" << endl;

    cin >> inputInt;
    LHSSize = inputInt;
    CoVar LHS [LHSSize];
    for (int i = 0; i < LHSSize; i++)
    {
        LHS[i].setN(nVars);
        LHS[i].setCoVar();
        if(LHS[i].getVar() == 'C')
        {
            *temp = *temp  + LHS[i].getCo();
        }
        else if(LHS[i].getVar() == 'X')
        {
            *(temp+1)  = *(temp+1) + -1*LHS[i].getCo();
        }
        else if(LHS[i].getVar() == 'Y')
        {
            *(temp+2) = *(temp+2)+ -1*LHS[i].getCo();
        }
        else
        {
            *(temp+3) = *(temp+3) + -1*LHS[i].getCo();
        }
    }

    cout << "This is the Left Hand Side you've input: " << endl;

    for(int i = 0; i < LHSSize; i++)
    {
        LHS[i].printCoVar();
        if(i < LHSSize-1)
        {
            cout << " + ";
        }
        else
        {
            cout << " = " << endl;
        }
    }

    cout << "How many inputs will you have on the Right Hand Side?" << endl;

    cin >> inputInt;
    RHSSize = inputInt;
    CoVar RHS [RHSSize];
    for (int i = 0; i < RHSSize; i++)
    {
        RHS[i].setN(nVars);
        RHS[i].setCoVar();
        if(RHS[i].getVar() == 'C')
        {
            *temp = *temp + -1*RHS[i].getCo();
        }
        else if(RHS[i].getVar() == 'X')
        {
            *(temp+1) = *(temp+1) + RHS[i].getCo();
        }
        else if(RHS[i].getVar() == 'Y')
        {
            *(temp+2) = *(temp+2) + RHS[i].getCo();
        }
        else
        {
            *(temp+3) = *(temp+3)+ RHS[i].getCo();
        }
    }

    cout << "This is the Right Hand Side you've input:" << endl;
    cout << " = ";
    for(int i = 0; i < RHSSize; i++)
    {
        RHS[i].printCoVar();
        if(i < RHSSize-1)
        {
            cout << " + ";
        }
    }

    cout << endl;

    cout << "This is your full equation: " << endl;

    for(int i = 0; i < LHSSize; i++)
    {
        LHS[i].printCoVar();
        if(i < LHSSize-1)
        {
            cout << " + ";
        }
        else
        {
            cout << " = ";
        }
    }
    for(int i = 0; i < RHSSize; i++)
    {
        RHS[i].printCoVar();
        if(i < RHSSize-1)
        {
            cout << " + ";
        }
    }
    cout << endl;

    cout << "And this is the simplified version of it: " << endl;
    for(int i = 0; i < nVars+1; i++)
    {
        cout << temp[i];
        if(i == 0)
        {
            cout  << " = ";
        }
        else if(i == 1)
        {
            cout << "X";
        }
        else if(i == 2)
        {
            cout << "Y";
        }
        else
        {
            cout << "Z";
        }
        if(i < nVars && i != 0)
        {
            cout << " + ";
        }
    }

    cout << endl;
}

void printSystemOfEquations (double *constantPointer, double *solutionMatrix, int dimension)
{
    for(int i = 0; i < dimension; i++)
        {
            cout << " |";
            for (int j = 0; j < dimension; j++)
            {
                if(j < dimension)
                {
                cout << " " << *(constantPointer + dimension*i + j);
                }
            }
            cout << " |";
            if( i == dimension/2)
            {
                cout << " * ";
            }
            else
            {
                cout << "   ";
            }
            cout << " | X" << i << " | ";
            if( i == dimension/2 )
            {
                cout << " = ";
            }
            else
            {
                cout << "   ";
            }
            cout << " | " << solutionMatrix[i] << " |" << endl;
        }
}

double calculateDet(double *constantPointer, double *solutionMatrix, int dimension)
{
    int position = 0;
    double currentDet = 1;
    double detArrayPosive[dimension];
    double detArrayNegative[dimension];
    if(dimension == 1)
    {
        cout << "X = " << *solutionMatrix / *constantPointer << endl;
        return (*solutionMatrix / *constantPointer);
    }
    else
    {
            for(int i = 0; i < dimension; i++)
            {
                for(int j = 0; j < dimension;j++)
                {
                    position = i*(dimension)+j*((dimension*dimension-1)/(dimension-1));
                    if(position >= dimension*dimension)
                    {
                        position -= dimension*dimension;
                    }
                    currentDet *= *(constantPointer+position);
                }
            detArrayPosive[i] = currentDet;
            currentDet = 1;
            }

            for(int i = 0; i < dimension; i++)
            {
                for(int j = 0; j < dimension;j++)
                {
                    position = i*(dimension)+j*(dimension-1)+(dimension-1);
                    if(position >= dimension*dimension )
                    {
                        position = position-dimension*dimension;
                    }

                    currentDet *= *(constantPointer+position);
                }
            detArrayNegative[i] = currentDet;
            currentDet = 1;
            }
        }

    currentDet = 0;

    if(dimension == 2)
    {
        currentDet = detArrayPosive[0] - detArrayPosive[1];
        return currentDet;
    }
    else
    {
        for(int i = 0; i < dimension; i++)
        {
            currentDet += detArrayPosive[i] - detArrayNegative[i];
        }
        return currentDet;
    }
}